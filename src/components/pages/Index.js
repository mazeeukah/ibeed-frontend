import React, { Component } from 'react';
import FacebookLogin from 'react-facebook-login';
import { GoogleLogin } from 'react-google-login';
// import { withAuth } from '@okta/okta-react';
import Notifications, { notify } from 'react-notify-toast';
import { withRouter } from "react-router-dom";
import * as API from '../../utils/api.js'
import auth from '../../utils/auth.js';
import { Button } from 'semantic-ui-react'
// import OktaSignIn from '@okta/okta-signin-widget';

const myColor = { background: '#0E1717', text: "#FFFFFF" };

class Index extends Component {


  constructor(props) {
    super(props);

    // this.widget = new OktaSignIn({
    //   baseUrl: 'https://dev-339192-admin.oktapreview.com',
    //   clientId: '0oaduxem9ju03V4JW0h7',
    //   redirectUri: 'http://localhost:3000/login',
    //   authParams: {
    //     responseType: 'id_token'
    //   }
    // });

    this.state = {
      // authenticated: null,
      username: '',
      password: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.login = this.login.bind(this);

  }






  handleChange(e) {
    e.target.classList.add('active');

    this.setState({
      [e.target.name]: e.target.value
    });



    // this.showInputError(e.target.name);
  }

  login(e) {
    e.preventDefault();
    // Here, we call an external AuthService. We’ll create it in the next step
    API.Login(this.state)
      .then((res) => {
        
        if (res.status === 204) {
          notify.show("Authentication failed", "error", 5000, myColor);

        }
        else {
          notify.show("Authentication Successful", "success", 5000, myColor);
          // console.log(res)
          auth.authenticateUser(res.data.token);
          setTimeout(() => {
            this.props.history.push("/myaccount");
          }, 5003)


        }
      })
      .catch(function (err) {
        notify.show("ERROR !!!", "error", 5000, myColor);
        console.log('Error logging in', err);
      });
  }

  responseGoogle(response) {


    API.googleLogin(response.profileObj).then(res => {
   
      notify.show("Authentication Successful", "success", 3000, myColor);
      auth.authenticateUser(res.data.token);
      window.location.href = "/myaccount"


    })
      .catch(function (err) {
        console.log(err)
      })


  };


  responseGoogleFailure() {


    notify.show("Authentication failed", "error", 5000, myColor);


  }

  responseFacebook(response) {
  


    API.facebookLogin(response).then(res => {

      notify.show("Authentication Successful", "success", 3000, myColor);
      auth.authenticateUser(res.data.token);
      window.location.href = "/myaccount"
     
    })
      .catch(function (err) {
        console.log(err)
      })

  }

  render() {

    return (
      <div>
        <Notifications />
        <section id="header">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 text-center col-xs-12 col-sm-12">
                <h2>iBeed Mari</h2>
                <h3>LOGIN</h3>
              </div>
            </div>
          </div>
        </section>

        <section id="content">
          <div className="container">
            <div className="row">
              <form className="account-form" onSubmit={this.login}>
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <input type="email" placeholder="E-mail"
                    id="email"
                    name="email"
                    ref="email"
                    value={this.state.email || ''}
                    onChange={this.handleChange}
                    required />
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <input type="password" placeholder="Password"
                    id="password"
                    name="password"
                    // ref="password"
                    value={this.state.password || ''}
                    onChange={this.handleChange}
                    required />
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12">

                  <input type="submit" value="SIGN IN" />
                </div>
                <div className="col-lg-12 text-center col-sm-12 col-xs-12">
                  <label>or sign in with socials</label>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <a className="btn-success"><i className="fa fa-facebook" aria-hidden="true"></i>
                    <FacebookLogin
                      appId="1412453065498943"
                      fields="name,email,picture"
                      callback={this.responseFacebook}
                    />
                  </a>
                </div>
                <div className="col-lg-12 col-sm-12 col-xs-12">
                  <a className="btn-success google"> <i className="fa fa-google-plus" aria-hidden="true"></i>
                    <GoogleLogin
                      clientId="67168199750-f2t41naiaaq1orccfg142sui83ir1u3q.apps.googleusercontent.com"
                      buttonText="Login"
                      callback={this.responseGoogle}
                      onSuccess={this.responseGoogle}
                      onFailure={this.responseGoogleFailure}

                    /></a>
                </div>
                <div className="col-lg-12 col-sm-12 text-center col-xs-12">
                  <label>Don’t have an account? <a href="/signup">Sign Up</a></label>
                </div>
              </form>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default withRouter(Index);
