import React from 'react';
import { Route, Switch, Link } from 'react-router-dom';

export default class Insurance extends React.Component {
  render() {
    return (
      <div>
        <section id="header" className="dashboard">
          <div className="container">
            <div className="row">
              <div className="col-lg-12  col-xs-12 col-sm-12">
                <a className="account" href="#"><i className="fa fa-angle-left" aria-hidden="true"></i> Insurance Detail</a>
              </div>
            </div>
          </div>
        </section>

        <section id="content">
          <div className="insurance-details">
            <div className="container">
              <div className="row">
                <div className="col-xs-6 flush-right">
                  <h4>COMPANY</h4>
                  <h6>AmeriHealth New Jersey</h6>
                  <h4>POLICY NAME</h4>
                  <h6>Crystal Plan</h6>
                  <div className="col-xs-6 flush">
                    <h4 className="small-size">END DATE</h4>
                    <h6 className="small-size">26 Feb 2018 </h6>
                  </div>
                  <div className="col-xs-6 flush">
                    <h4 className="small-size">START DATE</h4>
                    <h6 className="small-size">26 Feb 2018 </h6>
                  </div>
                </div>
                <div className="col-xs-6">
                  <h4>OWNER NAME</h4>
                  <h6>Asuka Hideko</h6>
                  <h4>POLICY TYPE</h4>
                  <h6>Individual</h6>
                  <a href="#" className="btn-primary">
                    <span id="Open" className="detail-btn">DETAIL</span>
                    <span id="OpenTwo" className="back-btn"><i className="fa fa-angle-left" aria-hidden="true"></i> BACK</span>
                  </a>
                </div>
              </div>
            </div>
          </div>


          <div id="Detail">
            <div className="col-xs-12 flush-right">
              <div className="personal-details bottom-space normal">
                <div className="container">
                  <div className="row">
                    <div className="col-xs-2 flush">
                      <img src="images/user-1.png" alt="" />
                    </div>
                    <div className="col-xs-9 flush-right" style={{ paddingLeft: '5px' }}>
                      <div className="col-xs-12  flush">
                        <h5 className="m-top-flush">Ryûnosuke Kamiki</h5>
                        <h6>Son</h6>
                        <div className="btn-menu">
                          Administrator
                          <label className="switch">
                            <input type="checkbox" />
                            <span className="slider round"></span>
                          </label>
                        </div>
                        <div className="btn-menu pull-right">
                          Beneficiary
                          <label className="switch">
                            <input type="checkbox" />>
                            <span className="slider round"></span>
                          </label>
                        </div>
                      </div>
                      <div className="col-xs-12 flush" style={{ marginTop: '25px' }}>
                        <div className="col-xs-4 top-text flush">
                          Amount
                        </div>
                        <div className="col-xs-8 flush">
                          <input id="ex1" type="text" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div className="col-xs-12 flush-right">
              <div className="personal-details normal bottom-normal">
                <div className="container">
                  <div className="row">
                    <div className="col-xs-2 flush">
                      <img src="images/user-2.png" alt="" />
                    </div>
                    <div className="col-xs-9 flush-right" style={{ paddingLeft: '5px' }}>
                      <div className="col-xs-12  flush">
                        <h5 className="m-top-flush">Kaito Ishikawa</h5>
                        <h6>Sister</h6>
                        <div className="btn-menu">
                          Beneficiary
                          <label className="switch" style={{ marginLeft: '10px' }}>
                            <input type="checkbox" />
                            <span className="slider round"></span>
                          </label>
                        </div>
                      </div>
                      <div className="col-xs-12 flush" style={{ marginTop: '25px' }}>
                        <div className="col-xs-4 top-text flush">
                          Amount
                        </div>
                        <div className="col-xs-8 flush">
                          <input id="ex2" />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="Back">
            <div className="container">
              <img src="images/img-1.jpg" alt="" />
            </div>
          </div>
        </section>

        <section id="footer">
          <ul>
            <li className="col-xs-3 active">
              <Link to='/dashboard'><img src="images/icon-1.png" alt="" /><br />Dashboard</Link>

            </li>
            <li className="col-xs-3">
              <Link to='/myaccount'><img src="images/icon-2.png" alt="" /><br />Management</Link>
            </li>
            <li className="col-xs-3">
              <Link to='/account'><img src="images/icon-3.png" alt="" /><br />Settings</Link>

            </li>
            <li className="col-xs-3">
              <Link to='/faq'><img src="images/icon-4.png" alt="" /><br />FAQ</Link>
            </li>
          </ul>
        </section>

      </div>
    )
  }
}