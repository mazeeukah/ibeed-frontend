import React, { Component } from 'react';
import { Route, Switch, Link } from 'react-router-dom';


class InsuranceManagement extends Component {
    render() {
        return (
            <div>
                <section id="header" className="dashboard">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12  col-xs-12 col-sm-12">
                                <a className="account" href="/"><i className="fa fa-angle-left" aria-hidden="true"></i> Insurance Management</a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content">
                    <div className="container">
                        <div className="row">
                            <div className="top-head account">
                                <div className="select-cate">
                                    <div className="col-xs-12 normal-space">
                                        <div className="col-xs-12 normal-space">
                                            <h5>Select Category</h5>
                                        </div>
                                        <div className="col-xs-3 normal-space">
                                            <a href="/auto" className="btn-info"><span><img src="../images/mini-icon-1.png" alt="" /></span><br />Auto</a>
                                        </div>
                                        <div className="col-xs-3 normal-space">
                                            <a href="/life" className="btn-info"><span><img src="../images/mini-icon-2.png" alt="" /></span><br />Life</a>
                                        </div>
                                        <div className="col-xs-3 normal-space">
                                            <a href="/health" className="btn-info"><span><img src="../images/mini-icon-3.png" alt="" /></span><br />Health</a>
                                        </div>
                                        <div className="col-xs-3 normal-space">
                                            <a href="/pet" className="btn-info"><span><img src="../images/mini-icon-4.png" alt="" /></span><br />Pet</a>
                                        </div>
                                    </div>
                                </div>
                                <form className="account-form insurance">
                                    <div className="col-xs-6 p-right-normal">
                                        <input type="text" placeholder="Insurance" />
                                    </div>
                                    <div className="col-xs-6 p-left-normal">
                                        <input type="text" placeholder="Product" />
                                    </div>
                                    <div className="col-xs-6 p-right-normal">
                                        <input type="text" placeholder="Product" />
                                    </div>
                                    <div className="col-xs-6 p-left-normal">
                                        <input type="text" placeholder="Product" />
                                    </div>
                                    <div className="col-xs-6 p-right-normal">
                                        <input type="text" placeholder="Fee" />
                                    </div>
                                    <div className="col-xs-6 p-left-normal">
                                        <input type="text" placeholder="Fee" />
                                    </div>
                                    <div className="col-xs-6 p-right-normal">
                                        <input id="datepicker" type="datetime" placeholder="Select start date" />
                                    </div>
                                    <div className="col-xs-6 p-left-normal">
                                        <input id="datepickertwo" type="datetime" placeholder="Select end date" />
                                    </div>
                                    <div className="col-xs-12 pdf">
                                        <button type="pdf"><i className="fa fa-upload" aria-hidden="true"></i>&nbsp; Select PDF or image</button>
                                    </div>
                                    <div className="col-xs-12">
                                        <input type="submit" value="SUBMIT" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="footer">
                    <ul>
                        <li className="col-xs-3 active">
                            <Link to='/dashboard'><img src="images/icon-1.png" alt="" /><br />Dashboard</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/myaccount'><img src="images/icon-2.png" alt="" /><br />Management</Link>
                        </li>
                        <li className="col-xs-3">
                            <Link to='/account'><img src="images/icon-3.png" alt="" /><br />Settings</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/faq'><img src="images/icon-4.png" alt="" /><br />FAQ</Link>
                        </li>
                    </ul>
                </section>
            </div>
        );
    }
}

export default InsuranceManagement;
