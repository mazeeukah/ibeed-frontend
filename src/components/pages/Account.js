import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'
import auth from '../../utils/auth.js';
import * as API from '../../utils/api.js'
import App from '../App';
import { Link } from 'react-router-dom'

class Account extends Component {

    constructor(props) {
        super(props);

        this.state = {
            name: ' Asuka Hideko',
            insuranceNo: 5,
            email: '',
            photo: '../images/user.png'
        }
    }

    componentDidMount() {
        let token = auth.getToken();
        if (token) {
            let decoded = auth.decodeToken(token);
            API.getUser(decoded.id).then(res => {
                this.setState({
                    name: res.data.data.name,
                    email: res.data.data.email,
                    photo: res.data.data.photo
                })
            })
                .catch(err => {
                    console.log(err)
                })
        }

    }

    render() {

        return (
            <div>
                <section id="header" className="dashboard">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12  col-xs-12 col-sm-12">
                                <a className="account" href="/"><i className="fa fa-angle-left" aria-hidden="true"></i> My Account</a>
                            </div>

                        </div>
                    </div>
                </section>

                <section id="content">
                    <div className="profile">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-5">
                                    <a href="#">
                                        <img className="high" src={this.state.photo} alt="" />
                                        <span><img src="../images/camara.png" alt="" /></span>
                                    </a>
                                </div>
                                <div className="col-xs-7">
                                    <h4>{this.state.name}</h4>
                                    <h5>{this.state.insuranceNo} Insurance</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="container">
                        <div className="row">
                            <div className="top-head account">

                                <ul className="nav nav-tabs" role="tablist">
                                    <li role="presentation" className="col-xs-4 flush text-center">
                                        <a href="#profile" aria-controls="home" role="tab" data-toggle="tab">PERSONAL</a>

                                    </li>
                                    <li role="presentation" className="col-xs-4 flush text-center">
                                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Address</a>
                                    </li>
                                    <li role="presentation" className="active col-xs-4 flush text-center">
                                        <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Family</a>
                                    </li>
                                </ul>

                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane" id="home">
                                        <form className="account-form">
                                            <div className="col-xs-12 user">
                                                <input type="text" placeholder="Your name" />
                                            </div>
                                            <div className="col-xs-12 email">
                                                <input type="text" placeholder="E-mail" />
                                            </div>
                                            <div className="col-xs-12 phone">
                                                <input type="text" placeholder="Phone" />
                                            </div>
                                            <div className="col-xs-12 fax">
                                                <input type="text" placeholder="Fax" />
                                            </div>
                                            <div className="col-xs-12 password">
                                                <input type="password" placeholder="Password" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="submit" value="SAVE " />
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="profile">
                                        <form className="account-form address-details">
                                            <div className="col-xs-12">
                                                <input type="address" placeholder="Address" />
                                            </div>
                                            <div className="col-xs-6 p-right-normal">
                                                <input type="text" placeholder="City" />
                                            </div>
                                            <div className="col-xs-6 p-left-normal">
                                                <input type="code" placeholder="Zip code" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="text" placeholder="Birthday" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="tel" placeholder="Phone" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="submit" value="SAVE " />
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" className="tab-pane active" id="messages">
                                        <form className="account-form">
                                            <div className="col-xs-12 user">
                                                <input type="text" placeholder="Name" />
                                            </div>
                                            <div className="col-xs-12 relationship">
                                                <input type="text" placeholder="Relationship" />
                                            </div>
                                            <div className="col-xs-12 email">
                                                <input type="text" placeholder="E-mail" />
                                            </div>
                                            <div className="col-xs-12 phone">
                                                <input type="text" placeholder="Phone" />
                                            </div>
                                            <div className="col-xs-12 fax">
                                                <input type="text" placeholder="Fax" />
                                            </div>
                                            <div className="col-xs-12 password">
                                                <input type="password" placeholder="Password" />
                                            </div>
                                            <div className="col-xs-12 photo">
                                                <input type="text" placeholder="Photo" />
                                            </div>
                                            <div className="col-xs-8 flush-right">
                                                <label className="font-text">Beneficiary for insurance</label>
                                            </div>
                                            <div className="col-xs-4 text-right">
                                                <label className="switch">
                                                    <input type="checkbox" />
                                                    <span className="slider round"></span>
                                                </label>
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="submit" value="Add Family Member" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="footer">
                    <ul>
                        <li className="col-xs-3 active">
                            <Link to='/dashboard'><img src="images/icon-1.png" alt="" /><br />Dashboard</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/account'><img src="images/icon-2.png" alt="" /><br />Management</Link>
                        </li>
                        <li className="col-xs-3">
                            <Link to='/myaccount'><img src="images/icon-3.png" alt="" /><br />Settings</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/faq'><img src="images/icon-4.png" alt="" /><br />FAQ</Link>
                        </li>
                    </ul>
                </section>
            </div>
        );
    }
}

export default Account;
