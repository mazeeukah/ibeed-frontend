import React, { Component } from 'react';
import { Button } from 'semantic-ui-react'
import auth from '../../utils/auth.js';
import * as API from '../../utils/api.js'
import App from '../App';
import { Link } from 'react-router-dom'


class AccountTwo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: ' Asuka Hideko',
            insuranceNo: 5,
            email: '',
            photo: '../images/user.png'
        }
    }

    componentDidMount() {
        let token = auth.getToken();
        if (token) {
            let decoded = auth.decodeToken(token);
            API.getUser(decoded.id).then(res => {
                this.setState({
                    name: res.data.data.name,
                    email: res.data.data.email,
                    photo: res.data.data.photo
                })
            })
                .catch(err => {
                    console.log(err)
                })
        }

    }

    render() {
        return (
            <div>
                <section id="header" className="dashboard">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12  col-xs-12 col-sm-12">
                                <a className="account" href="#"><i className="fa fa-angle-left" aria-hidden="true"></i> My Account</a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content">
                    <div className="profile">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-5">
                                    <a href="#">
                                        <img className="high" src="images/user.png" alt="" />
                                        <span><img src="images/camara.png" alt="" /></span>
                                    </a>
                                </div>
                                <div className="col-xs-7">
                                    <h4>{this.state.name}</h4>
                                    <h5>5 Insurance</h5>
                                    <h6>7 Family Members</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="personal-details">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-5 flush-right">
                                    <h5>Ryûnosuke Kamiki</h5>
                                    <h6>Son</h6>
                                </div>
                                <div className="col-xs-7 flush-left text-right">
                                    <label className="switch">
                                        <input type="checkbox" />
                                        <span className="slider round"></span>
                                    </label>
                                    <h6>Beneficiary for insurance</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="personal-details">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-5 flush-right">
                                    <h5>Kaito Ishikawa</h5>
                                    <h6>Mother</h6>
                                </div>
                                <div className="col-xs-7 flush-left text-right">
                                    <label className="switch">
                                        <input type="checkbox" />
                                        <span className="slider round"></span>
                                    </label>
                                    <h6>Beneficiary for insurance</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="personal-details">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-5 flush-right">
                                    <h5>Mone Kamishiraishi</h5>
                                    <h6>Sister</h6>
                                </div>
                                <div className="col-xs-7 flush-left text-right">
                                    <label className="switch">
                                        <input type="checkbox" />
                                        <span className="slider round"></span>
                                    </label>
                                    <h6>Beneficiary for insurance</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="personal-details">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-5 flush-right">
                                    <h5>Aoi Yûki</h5>
                                    <h6>Sister</h6>
                                </div>
                                <div className="col-xs-7 flush-left text-right">
                                    <label className="switch">
                                        <input type="checkbox" />
                                        <span className="slider round"></span>
                                    </label>
                                    <h6>Beneficiary for insurance</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="footer">
                    <ul>
                        <li className="col-xs-3 active">
                            <Link to='/dashboard'><img src="images/icon-1.png" alt="" /><br />Dashboard</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/myaccount'><img src="images/icon-2.png" alt="" /><br />Management</Link>
                        </li>
                        <li className="col-xs-3">
                            <Link to='/account'><img src="images/icon-3.png" alt="" /><br />Settings</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/faq'><img src="images/icon-4.png" alt="" /><br />FAQ</Link>
                        </li>
                    </ul>
                </section>
            </div>
        );
    }
}

export default AccountTwo;
