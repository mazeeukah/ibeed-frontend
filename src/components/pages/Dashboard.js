import React, { Component } from 'react';
import { Route, Switch, Link } from 'react-router-dom';

class Dashboard extends Component {
    render() {
        return (
            <div>
                <section id="header" className="dashboard two">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12  col-xs-12 col-sm-12">
                                <a href="#">DASHBOARD <i className="fa fa-plus" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content">
                    <div className="container">
                        <div className="row">
                            <div className="top-head">

                                <ul className="nav nav-tabs" role="tablist">
                                    <li role="presentation" className="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">ALL</a></li>
                                    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Auto</a></li>
                                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Life</a></li>
                                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Health</a></li>
                                    <li role="presentation"><a href="#pet" aria-controls="pet" role="tab" data-toggle="tab">Pet</a></li>
                                </ul>

                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane active" id="home">
                                        <div className="details">
                                            <div className="col-xs-3 img-high flush-right"><img src="images/dash-icon-1.png" alt="" /></div>
                                            <div className="col-xs-9">
                                                <h6>Pet Insurance company</h6>
                                                <span>Plan : <b>Gold Plan</b></span>
                                                <span>Type : <b>Family</b></span>
                                                <h5><i className="fa fa-calendar" aria-hidden="true"></i> 26 Feb 2016  -  26 Feb 2018 </h5>
                                            </div>
                                        </div>
                                        <div className="details">
                                            <div className="col-xs-3 img-high flush-right"><img src="images/dash-icon-2.png" alt="" /></div>
                                            <div className="col-xs-9">
                                                <h6>Altius Health Plans</h6>
                                                <span>Plan : <b>Crystal Plan</b></span>
                                                <span>Type : <b>Pet</b></span>
                                                <h5><i className="fa fa-calendar" aria-hidden="true"></i> 26 Feb 2016  -  26 Feb 2018 </h5>
                                            </div>
                                        </div>
                                        <div className="details">
                                            <div className="col-xs-3 img-high flush-right"><img src="images/dash-icon-3.png" alt="" /></div>
                                            <div className="col-xs-9">
                                                <h6>AmeriHealth New Jersey</h6>
                                                <span>Plan : <b>Crystal Plan</b></span>
                                                <span>Type : <b>Pension</b></span>
                                                <h5><i className="fa fa-calendar" aria-hidden="true"></i> 26 Feb 2016  -  26 Feb 2018 </h5>
                                            </div>
                                        </div>
                                        <div className="details">
                                            <div className="col-xs-3 img-high flush-right"><img src="images/dash-icon-4.png" alt="" /></div>
                                            <div className="col-xs-9">
                                                <h6>Care First</h6>
                                                <span>Plan : <b>Staterter</b></span>
                                                <span>Type : <b>Individual</b></span>
                                                <h5><i className="fa fa-calendar" aria-hidden="true"></i> 26 Feb 2016  -  26 Feb 2018 </h5>
                                            </div>
                                        </div>
                                        <div className="details last">
                                            <div className="col-xs-3 img-high flush-right"><img src="images/dash-icon-1.png" alt="" /></div>
                                            <div className="col-xs-9">
                                                <h6>Companion Insurance Com.</h6>
                                                <span>Plan : <b>Staterter</b></span>
                                                <span>Type : <b>Individual</b></span>
                                                <h5><i className="fa fa-calendar" aria-hidden="true"></i> 26 Feb 2016  -  26 Feb 2018 </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="profile">...</div>
                                    <div role="tabpanel" className="tab-pane" id="messages">...</div>
                                    <div role="tabpanel" className="tab-pane" id="settings">...</div>
                                    <div role="tabpanel" className="tab-pane" id="pet">...</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="footer">
                    <ul>
                        <li className="col-xs-3 active">
                            <Link to='/dashboard'><img src="images/icon-1.png" alt="" /><br />Dashboard</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/account'><img src="images/icon-2.png" alt="" /><br />Management</Link>
                        </li>
                        <li className="col-xs-3">
                         
                            <a href="/myaccount"><img src="images/icon-3.png" alt="" /><br />Settings</a>
                        </li>
                        <li className="col-xs-3">
                            <Link to='/faq'><img src="images/icon-4.png" alt="" /><br />FAQ</Link>
                        </li>
                    </ul>
                </section>
            </div>
        );
    }
}

export default Dashboard;
