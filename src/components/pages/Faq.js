import React, { Component } from 'react';
import { Route, Switch, Link } from 'react-router-dom';

class Faq extends Component {
    render() {
        return (
            <div>
                <section id="header" className="dashboard">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12  col-xs-12 col-sm-12">
                                <a className="account" href="/dashboard"><i className="fa fa-angle-left" aria-hidden="true"></i> FAQ</a>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="content">
                    <div className="col-xs-12">
                        <input className="search" type="search" placeholder="Search" />
                    </div>
                    <div className="col-xs-12">
                        <ul className="item">
                            <li className="col-xs-3 "><a href="/auto">Auto</a></li>
                            <li className="col-xs-3"><a href="/life">Life</a></li>
                            <li className="col-xs-3"><a href="/health">Health</a></li>
                            <li className="col-xs-3"><a href="/pet">Pet</a></li>
                        </ul>
                    </div>
                    <div className="faq-details">
                        <div className="col-xs-12">
                            <div className="personal-details">
                                <div className="container">
                                    <div className="row">
                                        <h5>This is where question goes</h5>
                                        <h6>Also known as ‘Deductible’, the Policy Excess is the fixed amount of money that you have to pay for each and every claim you may have –</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12">
                            <div className="personal-details">
                                <div className="container">
                                    <div className="row">
                                        <h5>What is Policy Excess?</h5>
                                        <h6>The premium for super-fast and exotic cars is a little higher and the policy excess (deductible) is higher too based on the fact that these cars are very </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12">
                            <div className="personal-details">
                                <div className="container">
                                    <div className="row">
                                        <h5>Is there a No Claims Bonus for safe riders?</h5>
                                        <h6>QIC does not provide Comprehensive insurance policies for motorcycles which are older than five years</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="col-xs-12">
                            <div className="personal-details border-none">
                                <div className="container">
                                    <div className="row">
                                        <h5>This is where question goes</h5>
                                        <h6>The premium for super-fast and exotic cars is a little higher and the policy excess (deductible) is higher too</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="footer">
                    <ul>
                        <li className="col-xs-3 active">
                            <Link to='/dashboard'><img src="images/icon-1.png" alt="" /><br />Dashboard</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/myaccount'><img src="images/icon-2.png" alt="" /><br />Management</Link>
                        </li>
                        <li className="col-xs-3">
                            <Link to='/account'><img src="images/icon-3.png" alt="" /><br />Settings</Link>

                        </li>
                        <li className="col-xs-3">
                            <Link to='/faq'><img src="images/icon-4.png" alt="" /><br />FAQ</Link>
                        </li>
                    </ul>
                </section>
            </div>
        );
    }
}

export default Faq;
