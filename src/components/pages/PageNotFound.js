import React from 'react';
import {
    Button,
    Form,
    Grid,
    Header,
    Image,
    Message,
    Segment,
} from 'semantic-ui-react';



class PageNotFound extends React.Component {


    render() {
        return (
            <div>

                <section id="header">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12 text-center col-xs-12 col-sm-12">
                                <h2>iBeed Mari</h2>
                                <h3>Page not found</h3>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default PageNotFound;
