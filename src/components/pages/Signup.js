import React, { Component } from 'react';
import * as API from '../../utils/api.js';
import Notifications, { notify } from 'react-notify-toast';
import { withRouter } from "react-router-dom";

const myColor = { background: '#0E1717', text: "#FFFFFF" };



class Signup extends Component {



    constructor(props) {
        super(props);
        this.state = {
            authenticated: null,
            name: '',
            username: '',
            password: '',
            email: '',
            phone: ''
        };
        this.handleChange = this.handleChange.bind(this);
        this.signUp = this.signUp.bind(this);
    }

    signUp(e) {

        e.preventDefault();
        // Here, we call an external AuthService. We’ll create it in the next step
        API.Signup(this.state)
            .then((res) => {
                // console.log(res)
                if (res.status === 204) {
                    notify.show("Registration failed !!!", "error", 5000, myColor);

                }
                else {

                    notify.show("Registration Successful.Redirecting to Login page ...", "success", 5000, myColor);

                    setTimeout(() => {
                        this.props.history.push("/");
                    }, 5003)

                }
            })
            .catch(function (err) {
                notify.show("ERROR !!!", "error", 5000, myColor);
               
                console.log('Error logging in', err);
            });
    }


    handleChange(e) {
        e.target.classList.add('active');

        this.setState({
            [e.target.name]: e.target.value
        });

        // console.log(this.state)

        // this.showInputError(e.target.name);
    }

    showInputError(refName) {
        const validity = this.refs[refName].validity;
        const label = document.getElementById(`${refName}Label`).textContent;
        const error = document.getElementById(`${refName}Error`);
        const isPassword = refName.indexOf('password') !== -1;



        if (!validity.valid) {
            if (validity.valueMissing) {
                error.textContent = `${label} is a required field`;
            } else if (validity.typeMismatch) {
                error.textContent = `${label} should be a valid email address`;
            } else if (isPassword && validity.patternMismatch) {
                error.textContent = `${label} should be longer than 4 chars`;
            }
            return false;
        }

        error.textContent = '';
        return true;
    }


    render() {
        return (
            <div>
                <Notifications />
                <section id="header" >
                    <div className="container" style={{ height: 100 }}>
                        <div className="row">
                            <div className="col-lg-12 text-center col-xs-12 col-sm-12">
                                <h2>iBeed Mari</h2>
                                <h3>Signup</h3>
                            </div>
                        </div>
                    </div>
                </section>

                <section id="content">

                    <div className="container">
                        <div className="row">
                            <div className="top-head account">

                                <div className="tab-content">
                                    <div role="tabpanel" className="tab-pane" id="home">
                                        <form className="account-form" onSubmit={this.signUp}>
                                            <div className="col-xs-12 user">
                                                <input type="text" placeholder="Your name" />
                                            </div>
                                            <div className="col-xs-12 email">
                                                <input type="text" placeholder="E-mail" />
                                            </div>
                                            <div className="col-xs-12 phone">
                                                <input type="text" placeholder="Phone" />
                                            </div>

                                            <div className="col-xs-12 password">
                                                <input type="password" placeholder="Password" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="submit" value="SAVE " />
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" className="tab-pane" id="profile">
                                        <form className="account-form address-details">
                                            <div className="col-xs-12">
                                                <input type="address" placeholder="Address" />
                                            </div>
                                            <div className="col-xs-6 p-right-normal">
                                                <input type="text" placeholder="City" />
                                            </div>
                                            <div className="col-xs-6 p-left-normal">
                                                <input type="code" placeholder="Zip code" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="text" placeholder="Birthday" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="tel" placeholder="Phone" />
                                            </div>
                                            <div className="col-xs-12">
                                                <input type="submit" value="SAVE " />
                                            </div>
                                        </form>
                                    </div>
                                    <div role="tabpanel" className="tab-pane active" id="messages">
                                        <form className="account-form" onSubmit={this.signUp} >
                                            <div className="col-xs-12 user">
                                                <input type="text" placeholder="Name" id="name"
                                                    name="name"
                                                    ref="name"
                                                    value={this.state.name}
                                                    onChange={this.handleChange}
                                                    required />
                                                <div className="error" id="nameError" />
                                            </div>

                                            <div className="col-xs-12 email">
                                                <input type="text" placeholder="E-mail"
                                                    id="email"
                                                    name="email"
                                                    ref="email"
                                                    value={this.state.email}
                                                    onChange={this.handleChange}
                                                    required />
                                                <div className="error" id="nameError" />
                                            </div>
                                            <div className="col-xs-12 phone">
                                                <input type="text" placeholder="Phone"
                                                    id="phone"
                                                    name="phone"
                                                    ref="phone"
                                                    value={this.state.phone}
                                                    onChange={this.handleChange}
                                                    required
                                                />
                                                <div className="error" id="nameError" />
                                            </div>
                                            <div className="col-xs-12 fax">
                                                <input type="text" placeholder="Username"
                                                    id="username"
                                                    name="username"
                                                    ref="username"
                                                    value={this.state.username}
                                                    onChange={this.handleChange}
                                                    required
                                                />
                                                <div className="error" id="nameError" />
                                            </div>
                                            <div className="col-xs-12 password">
                                                <input type="password" placeholder="Password"
                                                    id="password"
                                                    name="password"
                                                    ref="password"
                                                    value={this.state.password}
                                                    onChange={this.handleChange}
                                                    required
                                                />
                                                <div className="error" id="nameError" />
                                            </div>

                                            <div className="col-xs-12">
                                                <input type="submit" value="Signup" />
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        );
    }
}

export default withRouter(Signup);
