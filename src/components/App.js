import React, { Component } from 'react';
import Dashboard from './pages/Dashboard';
import Insurance from './pages/Insurance';
import Faq from './pages/Faq';
import Index from './pages/Index';
import AccountTwo from './pages/My-account-two.js';
import Account from './pages/Account.js';
import Signup from './pages/Signup.js';
import NotFoundSection from './pages/PageNotFound.js'
import InsuranceManagement from './pages/Insurance-management';
import { Route, Switch } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path="/" component={Index} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/faq" component={Faq} />
        <Route exact path="/signup" component={Signup} />
        <Route exact path="/insurance" component={Insurance} />
        <Route exact path="/insurance-management" component={InsuranceManagement} />
        <Route exact path="/account" component={AccountTwo} />
        <Route exact path="/myaccount" component={Account}/>
     
        <Route path='*' component={NotFoundSection} />
      </Switch>
    );
  }
}

export default App;
