import axios from 'axios';



const BASE_URL =  'https://ibeed-api.herokuapp.com/' // 'http://localhost:5000/'// || 



export const getUser = (id) => {
    // console.log('sign up')
    // console.log(data)
    return axios.get(BASE_URL + `api/user/${id}`)
        .then(res => { return res })
        .catch(function (error) {
            console.log(error);
        });
}



export const Signup = (data) => {
    // console.log('sign up')
    // console.log(data)
    return axios.post(BASE_URL + 'api/signup', { data })
        .then(res => { return res })
        .catch(function (error) {
            console.log(error);
        });
}


export const Login = (data) => {

    return axios.post(BASE_URL + 'api/login', { data })
        .then(res => { return res })
        .catch(function (error) {
            console.log(error);
        });
}


export const googleLogin = (data) => {

    data.id = data.googleId
    return axios.post(BASE_URL + 'api/googlelogin', { data })
        .then(res => { return res })
        .catch(function (error) {
            console.log(error);
        });
}



export const facebookLogin = (data) => {

    console.log(data)
    return axios.post(BASE_URL + 'api/facebooklogin', { data })
        .then(res => { return res })
        .catch(function (error) {
            console.log(error);
        });
}



